'use strict';

module.exports = function(Testclob) {

    Testclob.observe("access", function ProcessAccess(ctx,next){
        console.log("aaa\toberve access begin...");
        
        console.log("aaa\tctx type="+typeof(ctx)+"\tvalue="+JSON.stringify(ctx));

        next();
        console.log("aaa\tobserve access end.");

    });
 

    Testclob.observe("before save",function ProcessBeforeSave(ctx, next){
        console.log("Before save begin...");
        
        let idInputFieldName="jsonId";
        let jsonInputFieldName="jsonCnt";

        let idFieldName="id";
        let jsonFieldName="json";

        if(ctx.instance)
        {
/*          //JUST FOR DEBUG            
            console.log("bbb\tvalue="+JSON.stringify(ctx.instance)+"\ttype="+typeof(ctx.instance));
            console.log("bbb\tjsonid="+ctx.instance[idInputFieldName]+"\tjsonid type="+typeof(ctx.instance[idInputFieldName]));
            console.log("bbb\tjsoncnt="+ctx.instance[jsonInputFieldName]+"\tjsoncnt type="+typeof(ctx.instance[jsonInputFieldName]));
*/            
            if(typeof(ctx.instance[idInputFieldName])==="string")
            {
                ctx.instance[idFieldName]=ctx.instance[idInputFieldName];
            }
            else
                ctx.instance[idFieldName]=JSON.stringify(ctx.instance[idInputFieldName]);

            if(typeof(ctx.instance[jsonInputFieldName])==="string")
                ctx.instance[jsonFieldName]=ctx.instance[jsonInputFieldName];
            else
                ctx.instance[jsonFieldName]=JSON.stringify(ctx.instance[jsonInputFieldName]);

/*          //JUST FOR DEBUG
            console.log("bbb\tafter saving, the id value="+ctx.instance[idFieldName]+
                        "\tits type="+typeof(ctx.instance[idFieldName])+
                        "\tthe length="+ctx.instance[idFieldName].length);
            console.log("bbb\tafter saving, the cnt value="+ctx.instance[jsonFieldName]+
                        "\t its type="+typeof(ctx.instance[jsonFieldName])+
                        "\tthe length="+ctx.instance[jsonFieldName].length);
*/                        
        }
        else
        {
/*          //JUST FOR DEBUG            
            console.log("BBB\tvalue="+JSON.stringify(ctx.data)+"\ttype="+typeof(ctx.data));
            console.log("BBB\tjsonid="+ctx.data[idInputFieldName]+"\ttype="+typeof(ctx.data[idInputFieldName]));
            console.log("BBB\tjsoncnt="+ctx.data[jsonInputFieldName]+"\ttype="+typeof(ctx.data[jsonInputFieldName]));
*/            
            if(typeof(ctx.data[idInputFieldName])==="string")
            {
                ctx.data[idFieldName]=ctx.data[idInputFieldName];
            }
            else
                ctx.data[idFieldName]=JSON.stringify(ctx.data[idInputFieldName]);

            if(typeof(ctx.data[jsonInputFieldName])==="string")
                ctx.data[jsonFieldName]=ctx.data[jsonInputFieldName];
            else
                ctx.data[jsonFieldName]=JSON.stringify(ctx.data[jsonInputFieldName]);
/*          //JUST FRO DEBUG      
            console.log("BBB\tafter saving, the id value="+ctx.data[idFieldName]+
                        "\tits type="+typeof(ctx.data[idFieldName])+
                        "\tthe length="+ctx.data[idFieldName].length);
            console.log("BBB\tafter saving, the cnt value="+ctx.data[jsonFieldName]+
                        "\t its type="+typeof(ctx.data[jsonFieldName])+
                        "\t the length="+ctx.data[jsonFieldName].length);
*/                        
        }
        next();
        console.log("Before save end.");
    });

    Testclob.observe("persist",function ProcessPersist(ctx,next){
        console.log("Testclob model, persist begin...");
     
        if(ctx.instance)
        {
            console.log("ccc\t"+ctx.instance);
            console.log("ccc\t"+ctx.instance.id);
            console.log("ccc\t"+ctx.instance.json);

        }
        else
        {
            console.log("ddd\tvalue="+ctx.data+"\ttype="+typeof(ctx.data));
            console.log("ddd\tvalue="+ctx.data.id+"\ttype="+typeof(ctx.data.id));
            console.log("ddd\tvalue="+ctx.data.json+"\ttype="+typeof(ctx.data.json));
        }
    /*    
        var jsonObj=ctx.data.json;
        var jsonObjStr=JSON.stringify(jsonObj);
        console.log("jsonObjStr is "+jsonObjStr);
        var bufObj=new Buffer(jsonObjStr);
        var bufObjStr=bufObj.toLocaleString();
        console.log("bufObjStr is "+bufObjStr);
        //ctx.data.json=bufObjStr;
    */
        next();
        console.log("Testclob model, persist end.");
    });    

    Testclob.observe("before delete",function handleBeforeDelete(ctx, next)
    {
        console.log("EEE\tbefore delete begin...");
        console.log("eee\ttype="+typeof(ctx)+" value="+JSON.stringify(ctx));
        next();
        console.log("EEE\tbefor delete end.");
    });
};